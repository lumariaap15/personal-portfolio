<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Ganador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userGanador = User::query()->where('puntuacion',100)->where('id',2)->first();
        if ($userGanador !== null) {
            return redirect('/acertijo/ganador');
        }
        return $next($request);
    }
}
