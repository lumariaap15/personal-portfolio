<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function formContacto(Request $request)
    {
        try{
            $this->dispatch(new SendEmail($request->all()));
            //Mail::to($user->email)->send(new ReservaLocal($request->all()));
            return response()->json('Bien! tu mensaje fue enviado', 200);
        }catch (\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }
}
