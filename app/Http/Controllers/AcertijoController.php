<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AcertijoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home()
    {
        $ganador = \App\User::query()->where('puntuacion',100)->where('id',2)->first();
        $ganadores = \App\User::query()->where('puntuacion',100)->get();
        return view('acertijo.home',compact('ganador','ganadores'));
    }

    public function getScreen($num)
    {
        return view('acertijo.partial'.$num);
    }

    public function respuesta($respuesta)
    {
        $userGanador = User::query()->where('puntuacion',100)->first();
        if($userGanador === null){
            try {
                if(intval($respuesta) === 0){
                    throw new \Exception('Mmm, hay algo mal con tu respuesta. Si no pusiste nada, tal vez deberías reconsiderarlo :)');
                }
                $respuestaCorrecta = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484];
                $numbers = [];
                $separadoPorComas = explode(",", trim($respuesta));
                foreach ($separadoPorComas as $item) {
                    $separadoPorGuion = explode("-", trim($item));
                    if (count($separadoPorGuion) === 2) {
                        $i = intval(trim($separadoPorGuion[0]));
                        while ($i <= intval(trim($separadoPorGuion[1]))) {
                            array_push($numbers, $i);
                            $i++;
                        }
                    } else {
                        array_push($numbers, intval($separadoPorGuion[0]));
                    }
                }
                $aciertos = 22 - count(array_diff($respuestaCorrecta, $numbers));
                if (count($numbers) <= 22) {
                    if($aciertos === 0){
                        $puntuacion = rand(0,3).'.'.rand(0,9).rand(0,9);
                    }else{
                        $puntuacion = ($aciertos / 22) * 100;
                    }
                } else {
                    $puntuacion = ($aciertos / count($numbers))*100;
                }
                $puntuacion = round($puntuacion,2);
                $user = \App\User::query()->findOrFail(auth()->user()->id);
                if($user->puntuacion < $puntuacion){
                    $user->puntuacion = $puntuacion;
                }
                $user->num_intentos = $user->num_intentos+1;
                $user->save();
                if($puntuacion < 100){
                    return response()->json(['message'=>'¡Sigue intentando! Tu puntuación fue de '.$puntuacion, 'puntuacion'=>$user->puntuacion, 'num_intentos'=>$user->num_intentos],500);
                }
                return response()->json(['message'=>'ganador'],500);
            }catch (\Exception $exception){
                return response()->json(['message'=>$exception->getMessage()],500);
            }
        }else{
            return response()->json(['message'=>'ganador'],500);
        }
    }
}
