require('./bootstrap');
/*
require('jquery/dist/jquery.min');
require('popper.js/dist/popper.min');
require('axios/dist/axios.min');
 */

import Vue from 'vue'
import App from './App.vue';
import '@fortawesome/fontawesome-free/css/all.min.css';

Vue.config.productionTip = false;

import BootstrapVue from 'bootstrap-vue';
var VueScrollTo = require('vue-scrollto');

// You can also pass in the default options
Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});

// Import component
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin
Vue.component('Loading',Loading)

Vue.use(BootstrapVue);

Vue.component('app', require('./App.vue').default);
const app = new Vue({
    el: '#app',
});


