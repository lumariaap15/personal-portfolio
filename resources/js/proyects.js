const projects = [
    {
        img: "/assets/design/computer.jpeg",
        categoria: "Diseños",
        modal:"lg"
    },
    {
        img: "/assets/design/ctem-pack.png",
        categoria: "Diseños",
        modal:"lg"
    },
    {
        img: "/assets/design/famialzate-pack.png",
        categoria: "Diseños",
        modal:"lg"
    },
    {
        img: "/assets/design/fotodeteccion-pack.png",
        categoria: "Diseños",
        modal:"lg"
    },
    {
        img: "/assets/design/programador.jpeg",
        categoria: "Diseños",
        modal:"lg"
    },
    {
        img: "/assets/design/tarjeta.jpg",
        categoria: "Diseños"
    },
    {
        img: "/assets/design/web2.png",
        categoria: "Diseños",
        modal:"lg"
    },
    {
        img: "/assets/design/zonag-pack.png",
        categoria: "Diseños",
        modal:"lg"
    },
    {
        img: "/assets/dev/ong.png",
        video: "/assets/dev/Ong.mp4",
        categoria: "Desarrollos",
        modal:"lg"
    },
    {
        img: "/assets/dev/famialzate.png",
        video: "/assets/dev/famialzate.mp4",
        categoria: "Desarrollos",
        modal:"lg"
    },
    {
        img: "/assets/dev/famiapp.png",
        video: "/assets/dev/famiapp.mp4",
        categoria: "Desarrollos",
        modal:"sm"
    },
    {
        img: "/assets/dev/zonag.jpeg",
        video: "/assets/dev/zonag.mp4",
        categoria: "Desarrollos",
        modal:"sm"
    },
    {
        img: "/assets/dev/Intranet.png",
        video: "/assets/dev/Intranet-video.mp4",
        categoria: "Desarrollos",
        modal:"lg"
    },
    {
        img: "/assets/dev/ceindetec.png",
        video: "/assets/dev/ceindetec.mp4",
        categoria: "Desarrollos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/chica.png",
        categoria: "Dibujos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/cataratas.jpeg",
        categoria: "Dibujos"
    },
    {
        img: "/assets/drawings/flor.jpeg",
        categoria: "Dibujos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/fresa.jpeg",
        categoria: "Dibujos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/laura.jpeg",
        categoria: "Dibujos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/manos.jpeg",
        categoria: "Dibujos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/rana.jpeg",
        categoria: "Dibujos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/salvacion.jpeg",
        categoria: "Dibujos",
        modal:"lg"
    },
    {
        img: "/assets/drawings/tigre.jpeg",
        categoria: "Dibujos",
        modal:"lg"
    },
];

export default projects;
