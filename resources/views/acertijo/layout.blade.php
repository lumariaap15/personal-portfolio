<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acertijo 05/20</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <style>
        .bootbox.modal .modal-content{
            background: red !important;
            color: white !important;
            font-weight: 700;
            font-size: 30px;
            padding: 20px;
            /*margin-top: 200px !important;*/
        }
        .close{
            opacity: 1 !important;
        }
        .close span{
            font-size: 50px !important;
        }

        /* LOADER */
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-ripple div {
            position: absolute;
            border: 4px solid #995DB5;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
        }
        .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
        }
        @keyframes lds-ripple {
            0% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 1;
            }
            100% {
                top: 0px;
                left: 0px;
                width: 72px;
                height: 72px;
                opacity: 0;
            }
        }
        #loader-container{
            height: 100%;
            width: 100%;
            z-index: 100;
            background: rgba(255,255,255,.8);
            position: fixed;
            top: 0;
            right: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>
    @yield('styles')
</head>
<body>
<div id="loader-container">
    <div class="lds-ripple"><div></div><div></div></div>
</div>
@yield('content')

<script src="{{asset('assets/jquery/jquery-3.5.0.min.js')}}"></script>
<script src="{{asset('assets/popper/src/popper.js')}}"></script>
<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/axios/axios.min.js')}}"></script>
<script src="{{asset('assets/toast/bootbox.all.min.js')}}"></script>
<script>
    $(window).on('load',function () {
        $("#loader-container").fadeOut('slow')
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    function animateCSS(element, animationName, speed, callback) {
        const node = document.querySelector(element)
        node.classList.add('animated', animationName, speed)

        function handleAnimationEnd() {
            node.classList.remove('animated', animationName)
            node.removeEventListener('animationend', handleAnimationEnd)

            if (typeof callback === 'function') callback()
        }

        node.addEventListener('animationend', handleAnimationEnd)
    }

    function alertError(message) {
        let dialog = bootbox.dialog(
            {
                message: message,
                //closeButton:false,
                size:'large',
                className:'animated rubberBand'
            }
        );
        setTimeout(function () {
            dialog.modal('hide');
        },5000)
    }

    function loading() {
        $("#loader-container").fadeIn('slow')
    }
    function loadingClose() {
        $("#loader-container").fadeOut('slow')
    }


</script>
@yield('scripts')
</body>
</html>
