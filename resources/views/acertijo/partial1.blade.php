<div class="animated fadeIn slower">
    <h2 style="font-size: 70px" class="text-center">Imagina...</h2>
    <p class="text-center">Un pasillo muy largo con muchas puertas (500 para ser exactos &#128556), y todas están cerradas.</p>
    <div id="puertas-slider">
        <div class="d-flex flex-nowrap" style="overflow-x: hidden;">
            <img class="mr-10" src="{{asset('assets/acertijo/puerta.png')}}" alt="" width="90px">
            <img class="mr-10" src="{{asset('assets/acertijo/puerta.png')}}" alt="" width="90px">
            <img class="mr-10" src="{{asset('assets/acertijo/puerta.png')}}" alt="" width="90px">
            <img class="mr-10" src="{{asset('assets/acertijo/puerta.png')}}" alt="" width="90px">
            <img class="mr-10" src="{{asset('assets/acertijo/puerta.png')}}" alt="" width="90px">
            <img class="mr-10" src="{{asset('assets/acertijo/puerta.png')}}" alt="" width="90px">
            <img class="mr-10" src="{{asset('assets/acertijo/puerta.png')}}" alt="" width="90px">
        </div>
    </div>
    <div class="row justify-content-center my-5">
        <button class="btn btn-primary" onclick="nextScreen('/acertijo/screen/2')">Siguiente</button>
    </div>
</div>
<script>
    animarPuertas()
    function animarPuertas() {
        let sliderContainer = $("#puertas-slider .d-flex");
        let firstImg = sliderContainer.find('img')[0];
        let leftPos = $(sliderContainer).scrollLeft() + 150;
        $(firstImg).clone().appendTo(sliderContainer);
        $(firstImg).clone().appendTo(sliderContainer);
        $(sliderContainer.find('img')[0]).remove();
        sliderContainer.animate({scrollLeft: leftPos},500, function () {
            animarPuertas()
        })
    }
</script>
