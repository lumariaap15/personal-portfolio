@extends('acertijo.layout')
@section('styles')
    <style>
        #circle-1{
            width: 300px;
            height: 300px;
            border:10px solid #995DB5;
            position: absolute;
            top: -100px;
            left: -150px;
            animation: 10s move infinite alternate-reverse;
            opacity: .4;
        }

        #circle-2{
            width: 200px;
            height: 200px;
            background:#995DB5;
            position: absolute;
            top: 550px;
            left: -100px;
            animation: 13s move infinite alternate;
            opacity: .4;
        }

        #circle-3{
            width: 400px;
            height: 400px;
            background:#995DB5;
            position: absolute;
            top: 10%;
            right: -200px;
            animation: 15s move infinite alternate-reverse;
            opacity: .4;
        }

        #circle-4{
            width: 400px;
            height: 400px;
            border:10px solid #995DB5;
            position: absolute;
            top: 30%;
            right: -100px;
            animation: 20s move infinite alternate;
            opacity: .4;
        }

    </style>
@endsection
@section('content')
    <div class="row justify-content-center align-items-center" style="min-height: 100vh; position: relative; overflow: hidden;">
        <div id="circle-1" class="rounded-circle"></div>
        <div id="circle-2" class="rounded-circle"></div>
        <div id="circle-3" class="rounded-circle"></div>
        <div id="circle-4" class="rounded-circle"></div>
        <div class="p-5">

            @if(auth()->user()->id !== $ganador->id)
                <h5 class="text-center">¡Oh no! Alguien más ya resolvió el acertijo</h5>
            <h2 class="text-bold text-center" style="font-size: 60px">El ganador fue <span class="text-purple">{{$ganador->name}}</span><br>&#128557; &#128557; &#128557;</h2>
            @else
                <h5 class="text-center">¡Acertijo resuelto!</h5>
            <h2 class="text-bold text-center" style="font-size: 60px"><span class="text-purple">{{$ganador->name}},</span> ¡Eres el ganador, Resolviste el acertijo de primeras!<br>&#129351; &#129351; &#129351;</h2>
            @endif
            <p class="text-center">¡¡¡ Gracias por tu participación !!! La respuesta era: <br>1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484<br>
                <a class="text-purple text-bold" target="_blank" href="{{asset('assets/acertijo/acertijo.mp4')}}">CLICK AQUÍ PARA VER LA EXPLICACIÓN</a></p>
        </div>
    </div>
@endsection
