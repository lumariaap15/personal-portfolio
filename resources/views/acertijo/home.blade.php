@extends('acertijo.layout')
@section('styles')
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700,900|Raleway:300,400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Gloria+Hallelujah|Pacifico&display=swap" rel="stylesheet">
    <style>
        #circle-1{
            width: 300px;
            height: 300px;
            border:10px solid #995DB5;
            position: absolute;
            top: -100px;
            left: -150px;
            animation: 10s move infinite alternate-reverse;
            opacity: .4;
        }

        #circle-2{
            width: 200px;
            height: 200px;
            background:#995DB5;
            position: absolute;
            top: 550px;
            left: -100px;
            animation: 13s move infinite alternate;
            opacity: .4;
        }

        #circle-3{
            width: 400px;
            height: 400px;
            background:#995DB5;
            position: absolute;
            top: 10%;
            right: -200px;
            animation: 15s move infinite alternate-reverse;
            opacity: .4;
        }

        #circle-4{
            width: 400px;
            height: 400px;
            border:10px solid #995DB5;
            position: absolute;
            top: 30%;
            right: -100px;
            animation: 20s move infinite alternate;
            opacity: .4;
        }

    </style>
    <style>
        #container-presentacion{
            width: 100vw;
            background: white;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            overflow-y: scroll;
        }

        #container-presentacion .fondo{
            position: absolute;
            bottom: -30%;
            left: 0;
            width: 100%;
            z-index: -1;
        }

        #puertas-slider{
            width: 100%;
            border-bottom: 20px solid black;
            position: relative;
        }

        .mr-10{
            margin-right: 150px;
        }

        .door-container.bordered{
            border: 4px solid #995DB5;
            box-shadow: 0 0 30px #995DB5;
        }

        .door{
            transition: transform .3s;
        }
        .door.opened{
            /*transform: rotateY(80deg) translateX(-220px);*/
            animation: openDoor .3s;
            animation-fill-mode: forwards;
        }
        .door-container{
            width: 190px;
            height: 160px;
            margin-right: 60px;
        }
        .door-container img{
            position: absolute;
            left: 0;
            bottom: 0;
            width: 90px;
        }
        .door-container img.door{
            z-index: 10;
        }
        .door-label{
            font-size: 24px;
            z-index: 11;
            position: absolute;
            width: 30px;
            height: 30px;
            top: -20px;
            left: 25px;
            border-radius: 50%;
            padding: 10px;
            background: #995DB5;
            border: 3px solid black;
            color: white;
            margin-bottom: 0 !important;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        #img-hombre{
            position: absolute;
            z-index: 20;
            bottom: 0;
            left: -150px;
        }

        @keyframes openDoor {
            50%{
                transform: rotateY(80deg) translateX(-180px);
            }100%{
                 transform: rotateY(80deg) translateX(-220px);
             }
        }
        #counter{
            font-size: 80px;
        }

        @media (max-width: 575.98px) {
            #counter{
                font-size: 40px !important;
            }
        }


    </style>
@endsection
@section('content')
    <audio controls autoplay hidden loop id="audio">
        <source src="{{asset('assets/interstellar.ogg')}}" type="audio/ogg">
    </audio>
    <iframe src="{{asset('assets/click.mp3')}}" allow="autoplay" style="display: none"></iframe>
    <div class="container-fluid">
        @if(isset($ganador))
        <div id="pantalla-ganador" class="row justify-content-center align-items-center" style="min-height: 100vh; position: absolute; top: 0;left: 0;right: 0;z-index: 100;background: white; overflow: hidden;">
            <div id="circle-1" class="rounded-circle"></div>
            <div id="circle-2" class="rounded-circle"></div>
            <div id="circle-3" class="rounded-circle"></div>
            <div id="circle-4" class="rounded-circle"></div>
            <div class="p-5">

                @if(auth()->user()->id !== $ganador->id)
                    <h5 class="text-center">¡Oh no! Alguien más ya resolvió el acertijo</h5>
                    <h2 class="text-bold text-center" style="font-size: 60px">El ganador fue <span class="text-purple">{{$ganador->name}}</span><br>&#128557; &#128557; &#128557;</h2>
                @else
                    <h5 class="text-center">¡Acertijo resuelto!</h5>
                    <h2 class="text-bold text-center" style="font-size: 60px"><span class="text-purple">{{$ganador->name}},</span> ¡Eres el ganador, Resolviste el acertijo de primeras!<br>&#129351; &#129351; &#129351;</h2>
                @endif
                <p class="text-center">¡¡¡ Gracias por tu participación !!! La respuesta era: <br>1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400, 441, 484<br>
                    <a class="text-purple text-bold" target="_blank" href="{{asset('assets/acertijo/acertijo.mp4')}}">CLICK AQUÍ PARA VER LA EXPLICACIÓN</a><br>
                    Los siguientes usuarios también lograron resolverlo:<br>
                    @foreach($ganadores as $g)
                        {{$g->id !== 2 ? $g->name.', ' : ''}}
                        @endforeach
                </p>
                <div class="row justify-content-center my-3">
                    <div class="btn btn-primary" onclick="hideGanador()">Volver al acertijo</div>
                </div>
            </div>
        </div>
        @endif
        <div style="display: none" id="container-presentacion">
            <div class="p-3 mb-5" style="width: 100%">
                <div class="purplecard row justify-content-between" style="width: 100%">
                    <div class="col col-12 col-sm-auto">
                        <h5>Bienvenid@ {{explode(" ",auth()->user()->name)[0]}}, me alegra que te hayas sumado a este reto &#128522;</h5>
                    </div>
                    <div class="col col-12 col-sm-auto d-flex align-items-center">
                        <h5 class="text-bold mr-3">Intentos: <span id="num_intentos">{{auth()->user()->num_intentos}}</span> | Puntuación: <span id="puntuacion">{{auth()->user()->puntuacion}}</span></h5>
                        <form action="{{route('logout')}}" method="post">
                            @csrf
                            <button data-toggle="tooltip" data-placement="top" title="Cerrar sesión" class="btn btn-danger btn-sm" style="font-size: 25px">&#9940;</button>
                        </form>
                        <button onclick="togglePlay()" style="font-size: 25px" class="btn btn-info btn-sm ml-2 button-audio">&#128266;</button>
                    </div>
                </div>
            </div>
            <img class="fondo d-none d-sm-block" src="{{asset('assets/header-naranja.svg')}}" alt="">
            <div id="content-container" class="p-3" style="min-height: 100vh">

            </div>
            <div class="bg-black">
                <div class="container d-flex align-items-center justify-content-center" style="min-height: 300px">
                    <div>
                        <a href="/" target="_blank"><h1 class="text-white text-center mylogo">Lulú<span class="text-purple">:)</span></h1></a>
                        <p class="text-white text-center">Desarrollado con amor &#10084; por <a target="_blank" class="text-purple" href="https://www.linkedin.com/in/luisa-mar%C3%ADa-alzate-parrado-89a88a17a/">Luisa María Alzate</a>. 2020 &copy; Derechos reservados</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="container-counter" style="margin-left: -15px; margin-right: -15px; display: none">
            <div class="row justify-content-center align-items-center" style="min-height: 100vh; position: relative; overflow: hidden;">
                <div id="circle-1" class="rounded-circle"></div>
                <div id="circle-2" class="rounded-circle"></div>
                <div id="circle-3" class="rounded-circle"></div>
                <div id="circle-4" class="rounded-circle"></div>
                <div class="p-5" style="z-index: 10">
                    <h5 class="text-center text-bold">{{auth()->user()->name}},<br class="d-md-none"> el acertijo estará disponible en</h5>
                    <h2 id="counter" class="text-bold text-center"></h2>
                    <p class="text-center">¡Gracias por registrarte! Si no sabes que hacer mientras tanto, <br> un video de gatitos &#128049; &#128049; puede ayudarte:</p>
                    <div class="row flex-column align-items-center justify-content-center my-5">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/ibZLIeD8jjk?controls=0&autoplay=1&loop=1&mute=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture; loop;" ></iframe>
                        <form action="{{route('logout')}}" method="post" class="mt-4">
                            @csrf
                            <button class="btn btn-danger btn-sm">Cerrar sesión &#9940;</button>
                        </form>
                        <button onclick="togglePlay()" style="font-size: 25px" class="btn btn-info btn-sm mt-2 button-audio">&#128266;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $(function () {
            nextScreen('/acertijo/screen/1');
        });

        function hideGanador() {
            $("#pantalla-ganador").hide()
        }

        function nextScreen(screen) {
            //$("#content-container").fadeOut();
            $("#content-container").empty();
            $("#content-container").load(screen)
        }

        var myAudio = document.getElementById("audio");
        myAudio.volume = 0.3;
        var isPlaying = false;

        function togglePlay() {
            if (isPlaying) {
                myAudio.pause()
                $(".button-audio").html('&#128264;')
            } else {
                myAudio.play();
                $(".button-audio").html('&#128266;')
            }
        };
        myAudio.onplaying = function() {
            isPlaying = true;
        };
        myAudio.onpause = function() {
            isPlaying = false;
        };

    </script>
    <script>
        // Set the date we're counting down to
        var countDownDate = new Date("May 2, 2020 16:00:00").getTime();
        //var countDownDate = new Date("Apr 30, 2020 8:35:00").getTime();
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;
        if(distance > 0){
            $("#container-counter").fadeIn()
        }

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            now = new Date().getTime();

            // Find the distance between now and the count down date
            distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("counter").innerHTML = days + "<span class='text-purple'>d </span> : " + hours + "<span class='text-purple'>h </span> : "
                + minutes + "<span class='text-purple'>m </span> : " + seconds + "<span class='text-purple'>s </span>";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                $("#container-counter").fadeOut(500)
                $("#container-presentacion").fadeIn(1500)
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>
@endsection

