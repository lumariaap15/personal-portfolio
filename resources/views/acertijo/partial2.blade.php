<div class="animated fadeIn slower">
    <h2 style="font-size: 70px" class="text-center">Al paso de 1 hora... &#128336;</h2>
    <p class="text-center">...Llega un hombre y abre <b>todas</b> las puertas. <em>A continuación el ejemplo con las primeras 10.</em> <br><small class="d-md-none text-purple">(Parece que estás en un celular &#128580; Desliza las puertas hacia la izquierda para ver la animación completa)</small></p>
    <div style="border-bottom: 20px solid black;">
        <div class="d-flex flex-nowrap" style="overflow-x: scroll; position: relative; padding-top: 60px">
            <img id="img-hombre" src="{{asset('assets/acertijo/hombre-negro.png')}}" width="45px">
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">1</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">2</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">3</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">4</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">5</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">6</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">7</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">8</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">9</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
            <div class="door-container">
                <div style="position: relative; width: 90px; height: 100%">
                    <div class="door-label">10</div>
                    <img class="door" src="{{asset('assets/acertijo/puerta.png')}}">
                    <img src="{{asset('assets/acertijo/puerta-negra.png')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center my-5">
        <button class="btn btn-primary mr-3" onclick="nextScreen('/acertijo/screen/1')">&#8592; Atrás</button>
        <button class="btn btn-primary mr-3" onclick="nextScreen('/acertijo/screen/2')">Repetir &#128260;</button>
        <button class="btn btn-primary" onclick="nextScreen('/acertijo/screen/3')">Siguiente &#8594;</button>
    </div>
</div>
<script>
    $(function () {
        animarHombre();
    });
    var iterador = 0;
    function animarHombre() {
        let hombre = $("#img-hombre")
        let leftPos = Number(hombre.css('left').split('px')[0]) + 10;
        let nextImg = $($("body").find(".door")[iterador]);
        if(((iterador)*150) < leftPos){
            nextImg.addClass('opened');
            iterador++;
        }
        //iterador++;
        hombre.animate({bottom: 10 ,leftPos: leftPos - 100},50, function () {
            hombre.animate({bottom:0,left: leftPos},50, function () {
                if(iterador <= 10){
                    animarHombre()
                }
            })
        })
    }
</script>
