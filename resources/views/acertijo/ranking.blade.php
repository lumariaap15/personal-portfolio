@extends('acertijo.layout')
@section('content')
    <div class="p-5" style="background: #F5E1FF; min-height: 100vh">
        <h2>Ranking &#128221; &#128221; &#128221;</h2>
        <p>Última actualización: {{\Carbon\Carbon::now()->setTimezone('America/Bogota')}}</p>
        <div class="">
        <table class="table table-hover table-dark">
            <thead>
                <tr>
                    <th>N°</th>
                    <th>Nombre</th>
                    <th>Puntuación (0 A 100)</th>
                    <th>Número de intentos</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $key=>$user)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->puntuacion}}</td>
                    <td>{{$user->num_intentos}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>

@endsection
