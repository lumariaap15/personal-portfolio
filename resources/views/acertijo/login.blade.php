@extends('acertijo.layout')
@section('styles')
    <style>
        #circle-1{
            width: 300px;
            height: 300px;
            border:10px solid rgba(247,148,31,.4);
            position: absolute;
            top: -100px;
            left: -150px;
            animation: 10s move infinite alternate-reverse;
        }

        #circle-2{
            width: 200px;
            height: 200px;
            background:rgba(247,148,31,.4);
            position: absolute;
            top: 550px;
            left: -100px;
            animation: 13s move infinite alternate;
        }

        #circle-3{
            width: 400px;
            height: 400px;
            background:rgba(247,148,31,.4);
            position: absolute;
            top: 10%;
            right: -200px;
            animation: 15s move infinite alternate-reverse;
        }

        #circle-4{
            width: 400px;
            height: 400px;
            border:10px solid rgba(247,148,31,.4);
            position: absolute;
            top: 30%;
            right: -100px;
            animation: 20s move infinite alternate;
        }

        @keyframes move {
            to{
                transform: translate(20%, -50%);
            }
        }

        #titulo-bienvenido{
            transition: all 2s;
        }

    </style>
@endsection

@section('content')
    <audio controls autoplay hidden loop id="audio">
        <source src="{{asset('assets/interplanetary.ogg')}}" type="audio/ogg">
    </audio>
    <iframe src="{{asset('assets/click.mp3')}}" allow="autoplay" style="display: none"></iframe>
    <div class="container-fluid bg-extra-light-orange" style="position: relative; overflow: hidden">
        <div id="circle-1" class="rounded-circle"></div>
        <div id="circle-2" class="rounded-circle"></div>
        <div id="circle-3" class="rounded-circle"></div>
        <div id="circle-4" class="rounded-circle"></div>
        <div class="row justify-content-center align-items-center" style="min-height: 100vh">
            <div>
                <h2 id="titulo-bienvenido" style="font-size: 65px">Bienvenido al <span class="text-purple">Acertijo 05/20</span></h2>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-8 px-3" style="visibility: hidden" id="button-login">
                        <form action="{{route('login')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <input placeholder="Email" type="email" name="email" class="form-control">
                                @error('email')
                                <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input placeholder="Contraseña" type="password" name="password" class="form-control">
                                @error('password')
                                <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <button class="btn btn-primary btn-block" type="submit">Ingresar</button>
                        </form>
                        <a href="#" data-toggle="modal" data-target="#registro-modal" class="text-purple text-bold mt-3 d-block text-center">¿No tienes una cuenta? Regístrate</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="registro-modal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body bg-light-purple p-4">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title bg-light-purple">Registro</h2>
                    <form id="register-form">
                        @csrf
                        <div class="form-group">
                            <input placeholder="Nombre" name="name" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input placeholder="Celular" name="celular" type="number" minlength="10" maxlength="10" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input placeholder="Email" name="email" type="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input placeholder="Contraseña" name="password" type="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input placeholder="Confirmar Contraseña" name="password_confirmation" type="password" class="form-control" required>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Registrarse</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            //$("#audio").attr('autoplay',true)
            @if(count($errors) === 0)
            animationEntrance()
            @else
            $("#button-login").css('visibility','visible')
            @endif

        });
        function animationEntrance(){
            animateCSS('#titulo-bienvenido','fadeInUp','slower',function () {
                $("#titulo-bienvenido").css('padding-bottom',50)
                animateCSS('#button-login','fadeInUp','slower')
                $("#button-login").css('visibility','visible')
            })
        }
        $("#register-form").submit(function (e) {
            loading()
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(form[0]);
            axios.post('/register', formData)
                .then(function (response) {
                    loadingClose()
                    window.location.href = '/acertijo/home'
                })
                .catch(function (error) {
                    loadingClose()
                    let message = error.response;
                    if(message.data.errors !== undefined){
                        alertError(Object.values(message.data.errors)[0][0])
                    }else{
                        alertError(message)
                    }

                });
        });
    </script>
@endsection
