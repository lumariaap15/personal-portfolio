<div class="animated fadeIn slower">
    <p class="text-center">Y ahora... la pregunta del acertijo es (redobles por favor &#129345;):</p>
    <div class="row justify-content-center">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <h2 id="pregunta1" style="font-size: 50px; visibility: hidden" class="text-center" >¿Como se llamaba la mascota del hombre?</h2>
    <div class="d-flex justify-content-center">
        <iframe style="visibility: hidden" id="gif" src="https://giphy.com/embed/12msOFU8oL1eww" width="200" frameBorder="0" class="giphy-embed"></iframe>
    </div>
    <p id="paragraph2" class="text-center" style="visibility: hidden">Naaa, es un chiste &#128514;&#128514;&#128514;, ahora si va en serio:</p>
    <h2 id="pregunta2" style="font-size: 30px; visibility: hidden" class="text-center" >Suponiendo que las puertas están numeradas del 1 al 500, <br> y si el hombre continúa haciendo lo mismo cada hora, <br>¿qué puertas estarán abiertas al cabo de 500 horas?</h2>
    <div class="row justify-content-center my-5">
        <button class="btn btn-primary mr-3" onclick="nextScreen('/acertijo/screen/5')">&#8592; Atrás</button>
        <button class="btn btn-primary mr-3" onclick="nextScreen('/acertijo/screen/6')">Repetir &#128260;</button>
        <button class="btn btn-primary" onclick="nextScreen('/acertijo/screen/7')">Siguiente &#8594;</button>
    </div>
</div>
<script>
    $(function () {
        setTimeout(function () {
            animateCSS('#pregunta1','fadeInUp','slower',function () {
                setTimeout(function () {
                    animateCSS('#gif','heartBeat','faster', function () {
                        animateCSS('#paragraph2','fadeInUp','slower', function () {
                            animateCSS('#pregunta2','pulse','slower');
                            $("#pregunta2").css('visibility','visible')
                        });
                        $("#paragraph2").css('visibility','visible')
                    });
                    $("#gif").css('visibility','visible')
                },3000)
            });
            $(".spinner-border").hide()
            $("#pregunta1").css('visibility','visible')
        },5000)
    })
</script>
