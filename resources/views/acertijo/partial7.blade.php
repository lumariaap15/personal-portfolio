<div class="animated fadeIn slower">
    <h2 style="font-size: 50px;" class="text-center">Tu Respuesta</h2>
    <p class="text-center">Introduce los números de las puertas separados por comas</p>
    <form id="respuesta-form" action="/acertijo/respuesta" method="post">
        @csrf
        <div class="row justify-content-center">
            <div class="form-group">
                <input id="input-respuesta" style="min-width: 80vw" placeholder="Ej: 2-6,7,8,100-300" type="text" class="form-control">
            </div>
        </div>
        <div class="row justify-content-center my-5" id="buttons">
            <button class="btn btn-primary mr-3" type="button" onclick="nextScreen('/acertijo/screen/6')">Atrás</button>
            <button class="btn btn-primary" type="submit">Enviar</button>
        </div>
    </form>
    <p class="text-center">ATENCIÓN: La puntuación va de 0 a 100. Te puede indicar que tanto te acercaste a la respuesta, pero si envias más números de los que deberías, tu puntuación también bajará.</p>
</div>
<script>
    $("#respuesta-form").submit(function (e) {
        e.preventDefault();
        loading();
        var form = $(this);
        var formData = new FormData(form[0]);
        let input = $("#input-respuesta").val() === "" ? 0 : $("#input-respuesta").val()
        axios.post('/acertijo/respuesta/'+input, formData)
            .then(function (response) {
                loadingClose();
                console.log(response)
            })
            .catch(function (error) {
                loadingClose();
                if(error.response.data.message === 'ganador'){
                    window.location.reload()
                }else{
                    alertError(error.response.data.message)
                    if(error.response.data.puntuacion !== undefined){
                        $("#puntuacion").text(error.response.data.puntuacion)
                    }
                    if(error.response.data.num_intentos !== undefined){
                        $("#num_intentos").text(error.response.data.num_intentos)
                    }
                }
            });
    });
</script>
