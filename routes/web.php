<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function (){
    return view('welcome');
});
Route::get('/acertijo',function (){
   return redirect()->route('acertijo.home');
});
Route::get('/acertijo/login', function (){
    return view('acertijo.login');
})->name('acertijo.login')->middleware('guest');
Route::get('/acertijo/home', 'AcertijoController@home')->name('acertijo.home');
Route::post('/acertijo/respuesta/{respuesta}', 'AcertijoController@respuesta');
Route::get('/acertijo/screen/{num}', 'AcertijoController@getScreen');
Route::get('/acertijo/ganador', function (){
    $ganador = \App\User::query()->where('puntuacion',100)->where('id',2)->first();
    if($ganador ===  null){
        return redirect()->to('/acertijo/home');
    }
    return view('acertijo.ganador', compact('ganador'));
})->middleware('auth');
Route::get('/acertijo/ranking', function (){
    $users = \App\User::query()->orderBy('puntuacion','desc')->get();
    return view('acertijo.ranking', compact('users'));
});
Auth::routes();
